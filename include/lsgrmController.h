#ifndef __LSGRM_CONTROLLER_H
#define __LSGRM_CONTROLLER_H
#include "lsgrmGetInternalMemory.h"
#include "lsgrmSplitter.h"
#include "lsgrmGraphOperations.h"
#include "itkObject.h"
#include "itkMacro.h"


namespace lsgrm
{
template<class TSegmenter>
class ITK_EXPORT Controller : public itk::Object
{
public:

  /** Standard class typedef */
  typedef Controller Self;
  typedef itk::Object                   Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Runtime information support. */
  itkTypeMacro(Controller, itk::Object);

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /* Some convenient typedefs */
  using SegmenterType = TSegmenter;
  using ImageType = typename SegmenterType::ImageType;
  using LabelImageType = typename SegmenterType::LabelImageType;
  using SegmentationParameterType = typename SegmenterType::ParamType;
  using GraphType = typename SegmenterType::GraphType;

  /* Default constructor and destructor. */
  Controller();
  ~Controller();

  void Modified();

  void RunSegmentation();

  void SetSpecificParameters(const SegmentationParameterType& params);
  void SetInputImage(ImageType * inputImage);
  void SetInternalMemoryAvailable(long long unsigned int v); // expecting a value in Mbytes.

  /* Accessors */
  void SetTilingModeNone(){m_TilingMode = LSGRM_TILING_NONE; Modified();};
  void SetTilingModeUser(){m_TilingMode = LSGRM_TILING_USER; Modified();};
  void SetTilingModeAuto(){m_TilingMode = LSGRM_TILING_AUTO; Modified();};

  std::vector<std::string> GetTemporaryFilesList();

  itkGetMacro(Margin, unsigned int);

  itkGetMacro(TemporaryFilesPrefix, std::string);
  itkSetMacro(TemporaryFilesPrefix, std::string);

  itkGetMacro(Threshold, float);
  itkSetMacro(Threshold, float);

  itkGetMacro(NumberOfIterations, unsigned int);
  itkSetMacro(NumberOfIterations, unsigned int);

  itkGetMacro(NbTilesX, unsigned int);
  itkSetMacro(NbTilesX, unsigned int);

  itkGetMacro(NbTilesY, unsigned int);
  itkSetMacro(NbTilesY, unsigned int);

  itkGetMacro(NumberOfFirstIterations, unsigned int);
  itkSetMacro(NumberOfFirstIterations, unsigned int);

  itkGetMacro(TileWidth, unsigned int);
  itkSetMacro(TileWidth, unsigned int);

  itkGetMacro(TileHeight, unsigned int);
  itkSetMacro(TileHeight, unsigned int);

  itkGetMacro(OutputGraph, GraphType);

private:

  /** Enum for tiling mode */
  enum LSGRMTilingMode
  {
    LSGRM_TILING_NONE, // No tiling
    LSGRM_TILING_USER, // User tiling
    LSGRM_TILING_AUTO  // Automatic tiling
  };

  void GetAutomaticConfiguration();
  void ComputeMaximumStabilityMargin(unsigned int width, unsigned int height, unsigned int &iter, unsigned int &margin);
  void CheckMemorySize();
  unsigned int GetNodeMemory();
  std::size_t GetMaximumNumberOfNodesInMemory();

  /* Parameters given by the user */
  std::string m_TemporaryFilesPrefix;    // Path used to store intermediate files during the process.
  ImageType * m_InputImage;              // Input image

  /* Segmentation parameters */
  SegmentationParameterType m_SpecificParameters; // Specific parameters
  float                     m_Threshold;          // Threshold
  unsigned int              m_NumberOfIterations; // Number of iterations

  /* Parameters given by the user or computed automatically
   * depending of the chosen mode
   */
  long long unsigned int    m_Memory;                  // RAM available for the computation.
  unsigned int              m_NbTilesX;                // number of tiles in x dimension
  unsigned int              m_NbTilesY;                // number of tiles in y dimension
  unsigned int              m_NumberOfFirstIterations; // number of iterations in the first step
  unsigned int              m_TileWidth;               // regular tile width (i.e. not left tiles)
  unsigned int              m_TileHeight;              // regular tile height (i.e. not bottom tiles)

  /* read-only variables */
  LSGRMTilingMode                    m_TilingMode;         // tiling mode (none/user/auto)
  unsigned int                       m_Margin;             // stability margin related to m_NumberOfFirstIterations
  std::vector<ProcessingTile>        m_Tiles;              // list of tiles
  GraphType                          m_OutputGraph;        // Output graph

};
} // end of namespace lsgrm

#include "lsgrmController.txx"
#endif
