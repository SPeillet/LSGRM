#ifndef __LGSRM_FLS_SEGMENTER_H
#define __LGSRM_FLS_SEGMENTER_H
#include "lsgrmSegmenter.h"
#include "grmFullLambdaScheduleSegmenter.h"

namespace lsgrm
{

template<class TImage>
class FullLambdaScheduleSegmenter : public Segmenter<grm::FullLambdaScheduleSegmenter<TImage> >
{
public:

  /* Some convenient typedefs */
  typedef grm::FullLambdaScheduleSegmenter<TImage> Superclass;
  typedef typename Superclass::NodeType NodeType;
  typedef typename Superclass::EdgeType EdgeType;
  typedef typename Superclass::NodePointerType NodePointerType;


  FullLambdaScheduleSegmenter(){};

  void WriteSpecificAttributes(NodePointerType node, FILE * nodeStream);
  void ReadSpecificAttributes(NodePointerType node, FILE * nodeStream);
  long long unsigned int GetSpecificAttributesMemory(NodePointerType &node);
};

} // end of namespace lsrm
#include "lsgrmFullLambdaScheduleSegmenter.txx"
#endif
