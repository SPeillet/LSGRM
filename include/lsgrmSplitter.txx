#ifndef __LSGRM_SPLITTER_TXX
#define __LSGRM_SPLITTER_TXX
#include "lsgrmSplitter.h"

namespace lsgrm
{

enum RelativePosition{
  POS_TOP,
  POS_RIGHT,
  POS_BOTTOM,
  POS_LEFT
};

enum NeighborhoodRelativePosition{
  NBH_TOP,
  NBH_TOP_RIGHT,
  NBH_RIGHT,
  NBH_BOTTOM_RIGHT,
  NBH_BOTTOM,
  NBH_BOTTOM_LEFT,
  NBH_LEFT,
  NBH_TOP_LEFT
};

template <class TInputImage>
std::vector<ProcessingTile> SplitOTBImage(TInputImage * imagePtr, // input image
    unsigned int& tileWidth, // width of the tile
    unsigned int& tileHeight, // height of the tile
    const unsigned int margin, // stability margin
    unsigned int &nbTilesX,
    unsigned int &nbTilesY,
    std::string temporaryFilesPrefix)
    {

  std::vector<ProcessingTile> tiles;

  // Image size
  auto imageWidth = imagePtr->GetLargestPossibleRegion().GetSize()[0];
  auto imageHeight = imagePtr->GetLargestPossibleRegion().GetSize()[1];

  /* Local variables for the next loop */
  unsigned int startX, startY; // Upper left coordinates of the tile.
  unsigned int sizeX, sizeY; // Size of the tiles.

  /* Loop over the tiles*/
  tiles.assign(nbTilesX * nbTilesY, ProcessingTile());
  unsigned int i = 0;
  for(unsigned int row = 0; row < nbTilesY; ++row)
    {
    for(unsigned int col = 0; col < nbTilesX; ++col)
      {

      // Compute current tile start and size
      startX = col * tileWidth;
      startY = row * tileHeight;
      sizeX = tileWidth;
      sizeY = tileHeight;

      // Current tile size might be different for right and bottom borders
      if (col == nbTilesX -1)
        {
        sizeX += imageWidth % tileWidth;
        }
      if (row == nbTilesY -1)
        {
        sizeY += imageHeight % tileHeight;
        }

      /* Margin at the top ? */
      if( row > 0 )
        {
        tiles[i].margin[POS_TOP] = margin;
        tiles[i].rows[0] = row * tileHeight;
        tiles[i].tileNeighbors[NBH_TOP] = i - nbTilesX;
        }
      else
        {
        // Tile is on the top row --> no top margin
        tiles[i].margin[POS_TOP] = 0;
        tiles[i].rows[0] = 0;
        tiles[i].tileNeighbors[NBH_TOP] = -1;
        }

      /* Margin at the right */
      if( col < nbTilesX - 1 )
        {
        tiles[i].margin[POS_RIGHT] = margin;
        tiles[i].columns[1] = col * tileWidth + sizeX - 1; //sizeX
        tiles[i].tileNeighbors[NBH_RIGHT] = i+1;
        }
      else
        {
        // Tile is on the right column --> no right margin
        tiles[i].margin[POS_RIGHT] = 0;
        tiles[i].columns[1] = imageWidth - 1;
        tiles[i].tileNeighbors[NBH_RIGHT] = -1;
        }

      /* Margin at the bottom */
      if( row < nbTilesY - 1)
        {
        tiles[i].margin[POS_BOTTOM] = margin;
        tiles[i].rows[1] = row * tileHeight + sizeY - 1; // sizeY
        tiles[i].tileNeighbors[NBH_BOTTOM] = i + nbTilesX;
        }
      else
        {
        // Tile is on the bottom --> no bottom margin
        tiles[i].margin[POS_BOTTOM] = 0;
        tiles[i].rows[1] = imageHeight - 1;
        tiles[i].tileNeighbors[NBH_BOTTOM] = -1;
        }

      /* Margin at the left */
      if( col > 0 )
        {
        tiles[i].margin[POS_LEFT] = margin;
        tiles[i].columns[0] = col * tileWidth;
        tiles[i].tileNeighbors[NBH_LEFT] = i-1;
        }
      else
        {
        // Tile is on the left --> no left margin
        tiles[i].margin[POS_LEFT] = 0;
        tiles[i].columns[0] = 0;
        tiles[i].tileNeighbors[NBH_LEFT] = -1;
        }

      /* Store the tile region */
      typename TInputImage::IndexType index;
      index[0] = startX - tiles[i].margin[POS_LEFT];
      index[1] = startY - tiles[i].margin[POS_TOP];
      typename TInputImage::SizeType size;
      size[0] = sizeX + tiles[i].margin[POS_LEFT] + tiles[i].margin[POS_RIGHT];
      size[1] = sizeY + tiles[i].margin[POS_TOP] + tiles[i].margin[POS_BOTTOM];
      typename TInputImage::RegionType region(index, size);
      tiles[i].region = region;

      std::cout << "Tile " << i << ": start at " << index << " with size " << size << std::endl;

      /* Is there a neighbor at the rop right */
      if(row > 0 && col < nbTilesX - 1)
        tiles[i].tileNeighbors[NBH_TOP_RIGHT] = i - nbTilesX + 1;
      else
        tiles[i].tileNeighbors[NBH_TOP_RIGHT] = -1;

      /* Is there a neighbor at the bottom right */
      if(col < nbTilesX - 1 && row < nbTilesY - 1)
        tiles[i].tileNeighbors[NBH_BOTTOM_RIGHT] = i + nbTilesX + 1;
      else
        tiles[i].tileNeighbors[NBH_BOTTOM_RIGHT] = -1;

      /* Is there a neighbor at the bottom left */
      if(row < nbTilesY - 1 && col > 0)
        tiles[i].tileNeighbors[NBH_BOTTOM_LEFT] = i + nbTilesX - 1;
      else
        tiles[i].tileNeighbors[NBH_BOTTOM_LEFT] = -1;

      /* Is there a neighbor at the top left */
      if(col > 0 && row > 0)
        tiles[i].tileNeighbors[NBH_TOP_LEFT] = i - nbTilesX - 1;
      else
        tiles[i].tileNeighbors[NBH_TOP_LEFT] = -1;

      std::string suffix = std::to_string(row) + "_" + std::to_string(col) + ".bin";
      tiles[i].nodeFileName = temporaryFilesPrefix + "_node_" + suffix;
      tiles[i].edgeFileName = temporaryFilesPrefix + "_edge_" + suffix;
      tiles[i].nodeMarginFileName = temporaryFilesPrefix + "_nodeMargin_" + suffix;
      tiles[i].edgeMarginFileName = temporaryFilesPrefix + "_edgeMargin_" + suffix;

      i++;
      } // end for(unsigned int col = 0; col < nbTilesX; ++col)

    } // for(unsigned int row = 0; row < nbTilesY; ++row)

  return tiles;
    }
} // end of namespace lsgrm
#endif
