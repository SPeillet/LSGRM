#ifndef __LSGRM_SPLITTER_H
#define __LSGRM_SPLITTER_H
#include "lsgrmHeader.h"
#include "lsgrmGraphOperations.h"
#include "otbVectorImage.h"

namespace lsgrm
{

// Split with the OTB library
template <class TInputImage>
std::vector<ProcessingTile> SplitOTBImage(TInputImage * imagePtr, // input image
    unsigned int& tileWidth, // width of the tile
    unsigned int& tileHeight, // height of the tile
    const unsigned int margin, // stability margin
    unsigned int &nbTilesX,
    unsigned int &nbTilesY,
    std::string temporaryFilesPrefix);

} // end of namespace lsgrm

#include "lsgrmSplitter.txx"
#endif
