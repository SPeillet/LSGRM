#ifndef __LSRM_Spring_SEGMENTER_H
#define __LSRM_Spring_SEGMENTER_H
#include "lsgrmSegmenter.h"
#include "grmSpringSegmenter.h"

namespace lsgrm
{

template<class TImage>
class SpringSegmenter : public Segmenter<grm::SpringSegmenter<TImage> >
{
public:

  /* Some convenient typedefs */
  typedef grm::SpringSegmenter<TImage> Superclass;
  typedef typename Superclass::NodeType NodeType;
  typedef typename Superclass::EdgeType EdgeType;
  typedef typename Superclass::NodePointerType NodePointerType;


  SpringSegmenter(){};

  void WriteSpecificAttributes(NodePointerType node, FILE * nodeStream);
  void ReadSpecificAttributes(NodePointerType node, FILE * nodeStream);
  long long unsigned int GetSpecificAttributesMemory(NodePointerType &node);
};

} // end of namespace lsrm
#include "lsgrmSpringSegmenter.txx"
#endif



