#ifndef __LSGRM_BAATZ_SEGMENTER_H
#define __LSGRM_BAATZ_SEGMENTER_H
#include "lsgrmSegmenter.h"
#include "grmBaatzSegmenter.h"

namespace lsgrm
{

template<class TImage>
class BaatzSegmenter : public Segmenter<grm::BaatzSegmenter<TImage> >
{
public:

  /* Some convenient typedefs */
  typedef grm::BaatzSegmenter<TImage> Superclass;
  typedef typename Superclass::NodeType NodeType;
  typedef typename Superclass::EdgeType EdgeType;
  typedef typename Superclass::NodePointerType NodePointerType;


  BaatzSegmenter(){};

  void WriteSpecificAttributes(NodePointerType node, FILE * nodeStream);
  void ReadSpecificAttributes(NodePointerType node, FILE * nodeStream);
  long long unsigned int GetSpecificAttributesMemory(NodePointerType &node);
};

} // end of namespace lsrm
#include "lsgrmBaatzSegmenter.txx"
#endif



