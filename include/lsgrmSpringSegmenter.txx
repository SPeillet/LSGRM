#ifndef __LSRM_Spring_SEGMENTER_TXX
#define __LSRM_Spring_SEGMENTER_TXX
#include "lsgrmSpringSegmenter.h"
namespace lsgrm
{

template<class TImage>
void
SpringSegmenter<TImage>::WriteSpecificAttributes(NodePointerType node, FILE * nodeStream)
{
  std::size_t bands = node->m_Means.size();
  fwrite(&(bands), sizeof(bands), 1, nodeStream);

  for(unsigned int b = 0; b < node->m_Means.size(); b++)
    {
    fwrite(&(node->m_Means[b]), sizeof(node->m_Means[b]), 1, nodeStream);
    }
}

template<class TImage>
void
SpringSegmenter<TImage>::ReadSpecificAttributes(NodePointerType node, FILE * nodeStream)
{
  std::size_t bands;
  fread(&(bands), sizeof(bands), 1, nodeStream);
  node->m_Means.assign(bands, 0);

  for(unsigned int b = 0; b < bands; b++)
    {
    fread(&(node->m_Means[b]), sizeof(node->m_Means[b]), 1, nodeStream);
    }
}

template<class TImage>
long long unsigned int
SpringSegmenter<TImage>::GetSpecificAttributesMemory(NodePointerType &node)
{
  long long unsigned int memory = 0;

  memory += node->m_Means.size() * sizeof(float); // vector values
  memory += sizeof(std::vector<float>);           // vector container

  return memory;
}
} // end of namespace lsrm

#endif







